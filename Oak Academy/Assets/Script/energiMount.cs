﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class energiMount : MonoBehaviour
{
    Image energiValue;
    float maxStorage = 100f;

    public static float energi;

    int interval = 1;
    float nextTime = 0;

    public Text energn;

    void Start()
    {
        energiValue = GetComponent<Image>();
        energi = maxStorage;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextTime)
        {
            energi--;
            energiValue.fillAmount = energi / maxStorage;
            energn.text = energi.ToString();
            nextTime += interval;

        }
    }
}
