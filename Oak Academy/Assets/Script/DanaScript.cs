﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DanaScript : MonoBehaviour
{
    [SerializeField] Text danaScriptText;
    public static int danaAmount;
    void Start()
    {
        danaAmount = 0;
        danaScriptText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        danaScriptText.text = danaAmount.ToString();
    }
}
