﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SiswaSpawner : MonoBehaviour
{
    public GameObject siswa_Prefab;


    public void SpawnSiswa()
    {
        GameObject siswa_Obj = Instantiate(siswa_Prefab);

        Vector3 temp = transform.position;
        temp.z = 0f;
        siswa_Obj.transform.position = temp;
    }
}
