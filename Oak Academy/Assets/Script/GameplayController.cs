﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayController : MonoBehaviour
{
    public static GameplayController instance;
    public SiswaSpawner siswa_Spawner;

    [HideInInspector]
    public SiswaScript currentSiswa;

    private int moveCount;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Start()
    {
        siswa_Spawner.SpawnSiswa();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnNewSiswa()
    {
        Invoke("NewSiswa", 1f);
    }

    void NewSiswa()
    {
        siswa_Spawner.SpawnSiswa();
    }
}
