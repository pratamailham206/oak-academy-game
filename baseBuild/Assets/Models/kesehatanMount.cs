﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class kesehatanMount : MonoBehaviour
{
    Image kesehatanValues;
    float maxStorage = 100f;

    int interval = 1;
    float nextTime = 0;

    public static float keseh;
    public Text health;

    void Start()
    {
        kesehatanValues = GetComponent<Image>();
        keseh = maxStorage;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextTime)
        {
            keseh--;
            kesehatanValues.fillAmount = keseh / maxStorage;
            health.text = keseh.ToString();
            nextTime += interval;

        }        
    }
}
