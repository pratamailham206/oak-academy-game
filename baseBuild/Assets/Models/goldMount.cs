﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class goldMount : MonoBehaviour
{
    // Start is called before the first frame update
    Image goldValue;
    float maxStorage = 0f;

    int interval = 1;
    float nextTime = 0;

    public static float Gold;

    public Text dana;

    void Start()
    {
        goldValue = GetComponent<Image>();
        Gold = maxStorage;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextTime)
        {
            Gold++;
            dana.text = Gold.ToString();
            nextTime += interval;
        }
    }
}
