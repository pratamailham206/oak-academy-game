﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pendidikanMount : MonoBehaviour
{
    int interval = 1;
    float nextTime = 0;

    Image pendidikanValue;
    float maxStorage = 100f;

    public static float pend;

    public Text pendi;

    void Start()
    {
        pendidikanValue = GetComponent<Image>();
        pend = maxStorage;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextTime)
        {
            pend--;
            pendidikanValue.fillAmount = pend / maxStorage;
            pendi.text = pend.ToString();
            nextTime += interval;

        }
    }
}
